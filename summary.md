# 选择题错误总结

---
## 各类模拟题错误汇总
`*斜体为选错的*`
1. **垄断：**
   - *最简单的、初级的垄断组织是：短期价格协定*
   - 早期国际垄断同盟主要是：国际卡特尔
   - 当代国际垄断同盟的形式以国家垄断资本主义的国际联盟为主，如西方七国集团、欧盟等。
2. **经济全球化相关：**
   跨国公司为经济全球化提供了适宜的企业组织形式。

3. **扶贫：**
   - *中国特色减贫道路的鲜明特征：开发式扶贫方针*
   - 真抓实干、埋头苦干保证了脱贫攻坚战打的赢、打得好
   - 精准扶贫时打赢脱贫攻坚战的制胜法宝
4. **近代中国的各种阶级：**
   - *走向现代化和民主化的严重阻碍：封建地主阶级*
   - 帝国主义势力是近代中国贫穷落后和一切灾祸的总根源。
   - 民资客观上促进了中国的近代化
   - 农民阶级是中国民主革命的主力军
5. **共产党各个会议的主题：**
   `按时间顺序`
   - 中共一大：正式建立中国共产党
   - 中共二大：提出反帝反封建的民主革命纲领，采取群众路线
   - 中共三大：全体共产党以个人名义加入国民党，促进第一次国共合作
   - 中共四大：无产阶级在民主革命中的领导权问题和工农联盟的重要性
   - 八七会议：“枪杆子里出政权”，清算陈独秀右倾，确定土地革命，武装反抗国民党（从大革命到土地革命的转折）
   - 三湾改编：确立了党对军队的绝对领导
   - 遵义会议：解决了当时具有决定意义的组织问题和军事问题，确立了毛泽东的领导地位，党史上生死攸关的转折点，标志着党在政治上走向成熟
   - 瓦窑堡会议：提出抗日民族统一战线的新政策，批判左倾关门主义错误
   - 洛川会议：全面抗战路线，抗日救国十大纲领，敌后发动游击战，国统区群众运动
   - 六届六中全会：首次“马克思主义中国话”，确立“农村包围城市、武装夺取政权”（毛思想初步形成）
   - 中共七大：确立毛思想为指导思想；提出三大作风
   - 七届二中全会：1.迅速夺取全国胜利，天津式、北平式、绥远式；2.乡村到城市；3.农转工，新民主转社会主义；4.毛的两个务必（作风）
   - 七届三中全会：《为争取国家财政经济状况的基本好转而斗争》《不要四面出击》，建国后恢复国民经济
   - 中共八大：分析社改完成后的社会主要矛盾和主要任务。社会主义基本建立。集中力量发展生产力，经济上反保守反冒进，陈云“三个主体，三个补充”
   - 十一届三中全会：改革开放的序幕，否定“以阶级斗争为纲”，拨乱反正，重新确立思想、政治和组织路线，邓小平核心领导
   - 十一届六中全会：《历史决议》，第一次提“社会主义初级阶段”，科学评价毛，毛思想活的灵魂“实事求是、群众路线，独立自主”，完成拨乱反正
   - 中共十二大：提出“建设中特”的命题
   - 中共十三大：比较系统的阐述中特理论，邓理论轮廓形成；比较系统阐述社会主义初级阶段理论及基本路线
   - 中共十四大：经济改革目标"建立社会主义市场经济"
   - 中共十五大：基本经济制度“公有制为主体，多种所有制并存”
   - 中共十八大：指导思想“科学发展观”，战略目标“全面建成小康”
   - 中共十九大：指导思想“习近平新时代中特思想”

6. **著作：**
   > [马恩著作总结](https://zhuanlan.zhihu.com/p/25338611)
   >> **马克思：**
   1. 德意志意识形态：历史唯物主义观点
   2. 共产党宣言：1848年，共产主义者同盟，第一个无产阶级政党的党纲
   3. 法兰西内战：科学总结巴黎公社经验教训
   4. 资本论：剩余价值学说
   >> **恩格斯：**
   1. 反社林论：阐述马克思主义理论体系
   2. 家庭、私有制和国家的起源：古代社会发展规律和国家起源著作
   >> **列宁：**
   1. 国家与革命：恢复与捍卫被修正主义阉割的马克思主义
   2. 

7. **战争：**